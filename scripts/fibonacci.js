const fibonacci = (n) => {
  let sequence = [1, 1];

  for (let i = 2; i < n; i++) {
    sequence.push(sequence[i - 1] + sequence[i - 2]);
  }

  return sequence;
};

console.log(fibonacci(15));
