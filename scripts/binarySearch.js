const testArray1 = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const testArray2 = [11, 12, 21, 23, 40, 40, 42, 43, 54, 57, 92];
const testArray3 = [11, 12, 21, 23, 40, 40, 42, 43, 54, 57, 92];

function binarySearch(array, number) {
  let start = 0;
  let end = array.length - 1;

  while (start <= end) {
    const mid = Math.floor((start + end) / 2);

    if (array[mid] === number) {
      return mid;
    }

    if (number < array[mid]) {
      end = mid - 1;
    } else {
      start = mid + 1;
    }
  }

  return -1;
}

console.log(binarySearch(testArray1, 3));
console.log(binarySearch(testArray2, 54));
console.log(binarySearch(testArray3, 31));
