const company = ["Google", "Apple", "Microsoft"];
const ninjaTurtles = ["Leonardo", "Michelangelo", "Raphael", "Donatello"];
const scientist = ["Turing", 2, "Einstein", "Jung"];

const sortByStringLength = (array) => {

  const areAllStrings = array.every((element) => typeof element === "string");
  
  if (areAllStrings) return array.sort((a, b) => a.length - b.length);
};

console.log(sortByStringLength(ninjaTurtles));
console.log(sortByStringLength(company));
console.log(sortByStringLength(scientist));
