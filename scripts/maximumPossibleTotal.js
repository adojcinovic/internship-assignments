const testArray1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
const testArray2 = [1, 1, 0, 1, 3, 10, 10, 10, 10, 1];
const testArray3 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 100];

const maxPossibleTotal = (array) => {
    return array.sort((a, b) => b - a).slice(0, 5).reduce((acc, element) => acc + element);
}

console.log(maxPossibleTotal(testArray1));
console.log(maxPossibleTotal(testArray2));
console.log(maxPossibleTotal(testArray3));
