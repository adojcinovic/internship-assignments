const testArray1 = [1, 2, 3, 4, 6, 7, 8, 9, 10];
const testArray2 = [7, 2, 3, 6, 5, 9, 1, 4, 8];
const testArray3 = [10, 5, 1, 2, 4, 6, 8, 3, 9];

const missingNum = (array) => {
  const arrayOfTen = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10];
  const minuend = arrayOfTen.reduce((acc, element) => acc + element);
  const subtrahend = array.reduce((acc, element) => acc + element);

  return minuend - subtrahend;
};

console.log(missingNum(testArray1));
console.log(missingNum(testArray2));
console.log(missingNum(testArray3));
