const tableDiv = document.querySelector(".table");
const table = document.createElement("table");
const teams = [
  { name: "Team A", wins: 1, draws: 3, losses: 2 },
  { name: "Team B", wins: 2, draws: 1, losses: 1 },
  { name: "Team C", wins: 6, draws: 2, losses: 1 },
  { name: "Team D", wins: 7, draws: 4, losses: 1 },
  { name: "Team E", wins: 8, draws: 5, losses: 1 },
  { name: "Team F", wins: 9, draws: 1, losses: 1 },
  { name: "Team G", wins: 10, draws: 4, losses: 1 },
];

const getRanking = (teams) => {
  const sortedTeams = teams
    .map((element) => {
      element.points = element.wins * 3 + element.draws;
      return element;
    })
    .sort((a, b) => {
      if (a.points > b.points) return -1;
    });

  const firstRow = document.createElement("tr");
  const keys = Object.keys(sortedTeams[0]);

  keys.forEach((element) => {
    const field = document.createElement("th");
    field.innerHTML = element;
    firstRow.appendChild(field);
  });

  table.appendChild(firstRow);

  sortedTeams.forEach((team) => {
    const teamRow = document.createElement("tr");
    keys.forEach((key) => {
      const singleTeam = document.createElement("td");
      singleTeam.innerHTML = team[key];
      teamRow.appendChild(singleTeam);
    });
    table.appendChild(teamRow);
  });

  table.setAttribute("class", "table-bordered w-100");

  tableDiv.appendChild(table);
};

getRanking(teams);
