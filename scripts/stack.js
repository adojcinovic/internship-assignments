class Stack {
  items;
  count;

  constructor() {
    this.items = [];
    this.count = 0;
  }

  push(item) {
    this.items[this.count] = item;
    this.count++;

    return this.items;
  }

  pop() {
    const itemToRemove = this.items[this.count - 1];
    this.count -= 1;

    return itemToRemove;
  }

  peek() {
    return this.items[this.count - 1];
  }

  length() {
    return this.items.length;
  }

  search(item) {
    for (let i = 0; i < this.count; i++) {
      if (item === this.items[i]) {

        return this.items[i];
      }
    }
    return -1;
  }

  isEmpty() {
    return this.count === 0;
  }

  print() {
    let elements = '~';
    for (let i = 0; i < this.count; i++) {
        elements += this.items[i] + '~'
    }
    return elements;
  }
}

const stack = new Stack();

stack.push(2);
stack.push(4);
stack.push(5);
const x = stack.pop();
console.log(x);
console.log(stack.peek());
console.log(stack.length());
console.log(stack.search(4));
console.log(stack.isEmpty());
console.log(stack.print());
