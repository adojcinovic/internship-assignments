const inclusiveArray = (num1, num2) => {
  const array = [];

  if (num1 >= num2) {
    array.push(num1);
    return array;
  }
  array.push(num1);

  return array.concat(inclusiveArray(num1 + 1, num2));
};

console.log(inclusiveArray(2, 8));
