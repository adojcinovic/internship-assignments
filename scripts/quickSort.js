const quickSort = (array) => {
  if (array.length <= 1) {
    return array;
  }

  const pivot = array[array.length - 1];
  const lessThanPivot = [];
  const greaterThanPivot = [];

  for (let i = 0; i < array.length - 1; i++) {
    if (array[i] < pivot) {
      lessThanPivot.push(array[i]);
    } else {
      greaterThanPivot.push(array[i]);
    }
  }

  return [...quickSort(lessThanPivot), pivot, ...quickSort(greaterThanPivot)];
};

console.log(quickSort([2, 7, 1, 9, -2]));
