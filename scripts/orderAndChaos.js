const array = [
  "this",
  "will",
  "be",
  "extremely",
  "aeiou",
  "jjjjjj",
  "hard",
  "aatt",
];

const vowelsVsConsonants = (word) => {
  const vowels = "aeiou";
  let vowelCount = 0;
  let consonantCount = 0;

  word.split("").forEach((element) => {
    if (vowels.indexOf(element) !== -1) {
      vowelCount++;
    } else {
      consonantCount++;
    }
  });

  return vowelCount > consonantCount ? true : false;
};

const organize = (array) => {
  return array.reduce(
    (acc, element) => {
      if (element.length <= 2) {
        acc.xs.push(element);
      } else if (element.length >= 3 && element.length <= 5) {
        acc.s.push(element);
      } else if (element.length >= 6 && element.length <= 9) {
        acc.m.push(element);
      } else {
        acc.l.push(element);
      }

      if (element.length % 2 === 1) {
        acc.odd.push(element);
      } else {
        acc.even.push(element);
      }

      if (element.match(/[aeiou]/gi) !== null) {
        if (element.includes("y")) {
          if (
            element.match(/[aeiou]/gi).length >
            element.length - 1 - element.match(/[aeiou]/gi).length
          ) {
            acc.vowel.push(element);
          }
        }
        if (vowelsVsConsonants(element)) {
          acc.vowel.push(element);
        } else if (
          element.match(/[aeiou]/gi).length !==
          element.length - element.match(/[aeiou]/gi).length
        ) {
          acc.consonant.push(element);
        }
      }

      return acc;
    },
    { xs: [], s: [], m: [], l: [], odd: [], even: [], vowel: [], consonant: [] }
  );
};

console.log(organize(array));
