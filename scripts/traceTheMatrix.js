const matrix1 = [
  [1, 2],
  [3, 4],
];
const matrix2 = [
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9],
];

const trace = (array) => {
  let sum = 0;

  for (let i = 0; i < array.length; i++) {
    sum += array[i][i];
  }

  return sum;
};

console.log(trace(matrix1));
console.log(trace(matrix2));
